using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int VendaId { get; set; }
        public Vendedor Vendedor { get; set; }
        [DataType(DataType.Date)]
        public DateTime Data { get; set; }
        [Required, MinLength(1)]
        public ICollection<Item> Itens { get; set; }

        [DefaultValue(0)]
        [Range(0, 4, ErrorMessage = @"Digite um status válido: 0 - Aguardando pagamento, 1 - Pagamento aprovado, 2 - Enviado para transportadora, 3 - Entregue, 4 - Cancelada")]
        public EStatusDaVenda Status { get; set; }
    }
}