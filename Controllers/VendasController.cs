using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Data;
using tech_test_payment_api.Models;
namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly DataContext _context;

        public VendasController(DataContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> ObterPorId(int id)
        {
            var venda = await _context.Vendas.Include(x => x.Vendedor).Include(x => x.Itens).FirstOrDefaultAsync(x => x.VendaId == id);

            if (venda == null)
            {
                return NotFound();
            }
            return Ok(venda);
        }
        [HttpPost]
        public async Task<IActionResult> RegistrarVenda(Venda venda)
        {
            await _context.Vendas.AddAsync(venda);
            await _context.SaveChangesAsync();
            return Ok(venda);
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult> AtualizarStatus(int id, bool cancelar = false)
        {
            var venda = await _context.Vendas.Include(x => x.Vendedor).Include(x => x.Itens).FirstOrDefaultAsync(x => x.VendaId == id);

            if (venda == null)
            {
                return NotFound();
            }
            if (cancelar == false)
            {
                switch (venda.Status)
                {
                    case Enums.EStatusDaVenda.Aguardando:
                        venda.Status = Enums.EStatusDaVenda.Aprovada;
                        break;
                    case Enums.EStatusDaVenda.Aprovada:
                        venda.Status = Enums.EStatusDaVenda.Enviada;
                        break;
                    case Enums.EStatusDaVenda.Enviada:
                        venda.Status = Enums.EStatusDaVenda.Entregue;
                        break;
                }
            }
            if (cancelar == true)
            {
                switch (venda.Status)
                {
                    case Enums.EStatusDaVenda.Aguardando:
                        venda.Status = Enums.EStatusDaVenda.Cancelada;
                        break;
                    case Enums.EStatusDaVenda.Aprovada:
                        venda.Status = Enums.EStatusDaVenda.Cancelada;
                        break;
                    case Enums.EStatusDaVenda.Enviada:
                        return BadRequest("A venda já foi enviada e não pode ser cancelada");
                    case Enums.EStatusDaVenda.Entregue:
                        return BadRequest("A venda já foi entregue e não pode ser cancelada");

                }
            }

            await _context.SaveChangesAsync();
            return Ok($"O status da venda {venda.VendaId} foi atualizado para: {venda.Status}");
        }
    }
}
