using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Enums
{

    public enum EStatusDaVenda
    {
        Aguardando,
        Aprovada,
        Cancelada,
        Enviada,
        Entregue
    }
}